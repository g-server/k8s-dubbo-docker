#!/bin/bash
#setenforce 0
cur_path=$(cd "$(dirname "$0")"; pwd) #&& echo $cur_path
cmd=$1 && push=$2
printf "entry params: [%s, %s]\n" $cmd $push

repo=registry.cn-shenzhen.aliyuncs.com
docker login --username=${DOCKER_REGISTRY_USER} --password=${DOCKER_REGISTRY_PW} $repo

function buildPushImg(){
    namespace=$1
    img_name=$2
    is_push=$3
    img_ver=$4

    if [ "" = "$img_ver" ]; then
      img_ver=latest
    fi

    echo ">>>==============={{ "$img_name" }}>>>build:========================================="
    docker build -t $repo/$namespace/$img_name:$img_ver .

    if [ "push" = "$is_push" ]; then
        echo ">>>==============={{ "$img_name" }}>>>push:========================================="
        docker push $repo/$namespace/$img_name:$img_ver
    fi

}

function doOne(){
    local mod=$1
    #cd $cur_path/$mod && buildPushImg "k-app" "ct-um-$mod" "$push"
    cd $cur_path/$mod && buildPushImg "k-app" "k8s-dubbo-$mod" "$push"
}

case "$cmd" in
  01)
    #cd $cur_path/top/parent/img-base/           && buildPushImg "ym_top" "img-base" "$push"
    doOne service-producer
    doOne service-consumer
    exit $?
    ;;
  *)
    echo "params: [01, 02, 03]"
    exit 1
    ;;
esac	


